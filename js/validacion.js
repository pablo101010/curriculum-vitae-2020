function requerido(input) {
    if (input.value != "") {
        //el input tiene texto
        input.className = "form-control is-valid"
        return true;
    } else {
        //el input no tiene texto
        input.className = "form-control is-invalid"
        return false;
    }
}

function revisarEmail(input) {
    //pablo@gmail.com
    let expresion = /\w+@\w+\.[a-z]/;

    if (input.value != "" && expresion.test(input.value)) {
        input.className = "form-control is-valid";
        return true;
    } else {
        //el input no tiene texto
        input.className = "form-control is-invalid";
        return false;
    }
}

function revisarNumeros(input) {
    if (input.value != "" && !isNaN(input.value)) {
        console.log("esta bien")
        input.className = "form-control is-valid";
        return true;
    } else {
        input.className = "form-control is-invalid";
        return false;
    }
}

// function revisarConsulta(consulta) {
//     if (consulta.value.lenght >= 10) {
//         consulta.className = "form-control is-valid";
//     } else {
//         input.className = "form-control is-invalid";
//     }
// }

function revisarConsulta(consulta) {
    if ((consulta.value.length) > 10) {
        consulta.className = "form-control is-valid";
        return true;
    } else {
        consulta.className = "form-control is-invalid";
        return false;
    }
}

//agregar un evento a un objeto HTML
let checkTerminos = document.getElementById("terminos");
//Agregar eventos
checkTerminos.addEventListener("change", revisarTerminos);

function revisarTerminos() {
    if (checkTerminos.checked) {
        checkTerminos.className = "form-check-input is-valid";
        return true;
    } else {
        checkTerminos.className = "form-check-input is-invalid";
        return false;
    }
}

function validarGeneral(eventoo) {
    event.preventDefault();
    console.log("hola" + eventoo)

    if (requerido(document.getElementById("nombre")) &&
        revisarEmail(document.getElementById("email")) &&
        revisarNumeros(document.getElementById("telefono")) &&
        revisarConsulta(document.getElementById("consulta"))) {
        alert("El formulario fue enviado");
        enviarEmail()
    } else {
        alert("Ocurrio un error");
        
    }
}

function enviarEmail() {
    let template_params = {
        to_name: "Administrador",
        from_name: document.getElementById("nombre").value,
        message_html: `Telefono: ${document.getElementById("telefono").value} - 
        Email ${document.getElementById("email").value} - 
        Consulta: ${document.getElementById("consulta").value}`
    };
     
    var service_id = "default_service";
    var template_id = "template_cv";
    emailjs.send(service_id, template_id, template_params);
}